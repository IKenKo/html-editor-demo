<?php

namespace SolutionForest\PageBuilder\Http\Controllers;

use Encore\Admin\Layout\Content;
use Illuminate\Routing\Controller;
use SolutionForest\PageBuilder\PageBuilder;
use Request;

class PageBuilderController extends Controller
{
    public function saveAsset(){
        $files = Request::file("files");
        $builder = new PageBuilder();
        $storage = $builder->getStorage();
        $path = PageBuilder::config("path");
        foreach ($files as $file) {
            $storage->putFileAs($path, $file, $file->getClientOriginalName());
        }
        $data = collect($storage->files($path))->map(function($file) use ($storage){
            return $storage->url($file);
        });
        return response()->json(["data"=>$data]);
    }

    public function images(){
        $builder = new PageBuilder();
        $storage = $builder->getStorage();
        $path = PageBuilder::config("path");
        $data = collect($storage->files($path))->map(function($file) use ($storage){
            return $storage->url($file);
        });
        return response()->json(["data"=>$data]);
    }
}