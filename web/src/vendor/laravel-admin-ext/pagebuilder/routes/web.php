<?php

use SolutionForest\PageBuilder\Http\Controllers\PageBuilderController;

// Route::get('pagebuilder', PageBuilderController::class.'@index');
Route::post('pagebuilder/assets/save', PageBuilderController::class.'@saveAsset');
Route::get('pagebuilder/images', PageBuilderController::class.'@images');