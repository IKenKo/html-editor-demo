<?php

namespace App\Admin\Controllers;

use App\Models\HtmlPage;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class HtmlPageController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'HtmlPage';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new HtmlPage);
        $grid->column('id', 'ID')->sortable();
        $grid->column('created_at');
        $grid->column('updated_at');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(HtmlPage::findOrFail($id));
        $show->id('ID');
        $show->created_at();
        $show->updated_at();
        $show->page_html()->unescape();
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new HtmlPage);
        $form->pageBuilder('page_html');
        return $form;
    }
}
