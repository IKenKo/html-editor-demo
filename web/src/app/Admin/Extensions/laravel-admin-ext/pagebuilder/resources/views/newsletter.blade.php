@php
    $flatId = str_replace("-", "_", $id);    
@endphp
<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

        <label for="{{$id}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>
    
        <div class="{{$viewClass['field']}}">
            <button class="btn btn-success btn-sm" id="builder-btn" data-field="{{$flatId}}">{{__("News Letter Builder")}}</button>
            <button class="btn btn-default btn-sm" id="editor-btn" data-field="{{$flatId}}">{{__("Classic Editor")}}</button>
            
            @include('admin::form.error')
    
            <div class="editor-wrapper">
                <hr />
                <textarea class="form-control {{$class}} builder-editor" id="{{$flatId}}" name="{{$name}}" placeholder="{{ $placeholder }}" {!! $attributes !!} >{{ old($column, $value) }}</textarea>
            </div>
    
            @include('admin::form.help-block')
    
        </div>
    </div>
    <div class="page-builder-popup" data-field="{{$flatId}}">
        <div id="gjs-{{$flatId}}"></div>
    </div>
    