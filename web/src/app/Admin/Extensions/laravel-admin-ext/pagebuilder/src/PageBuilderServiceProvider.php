<?php

namespace SolutionForest\PageBuilder;

use Illuminate\Support\ServiceProvider;
use SolutionForest\PageBuilder\Fields\PageBuilderButton;
use SolutionForest\PageBuilder\Fields\NewsLetterBuilderButton;
use Encore\Admin\Form;
use Encore\Admin\Admin;

class PageBuilderServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(PageBuilder $extension)
    {
        if (! PageBuilder::boot()) {
            return ;
        }

        if ($views = $extension->views()) {
            $this->loadViewsFrom($views, 'pagebuilder');
        }

        if ($this->app->runningInConsole() && $assets = $extension->assets()) {
            $this->publishes(
                [$assets => public_path('vendor/laravel-admin-ext/pagebuilder')],
                'pagebuilder'
            );
        }

        $this->app->booted(function () {
            PageBuilder::routes(__DIR__.'/../routes/web.php');
            Form::extend('pageBuilder', PageBuilderButton::class);
            Form::extend('newsletterBuilder', NewsLetterBuilderButton::class);
        });
    }
}