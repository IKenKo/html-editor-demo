<?php

namespace SolutionForest\PageBuilder;

use Encore\Admin\Extension;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Encore\Admin\Exception\Handler;
use League\Flysystem\Adapter\Local;

class PageBuilder extends Extension
{
    
    public $name = 'pagebuilder';

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';

    public $menu = [];

    public $storage;

    public function getStorage(){
        if(empty($this->storage)){
            $disk = static::config('disk');
            $this->storage = Storage::disk($disk);

            if (!$this->storage->getDriver()->getAdapter() instanceof Local) {
                Handler::error('Error', '[laravel-admin-ext/pagebuilder] only works for local storage.');
            }
        }
        return $this->storage;
    }
}