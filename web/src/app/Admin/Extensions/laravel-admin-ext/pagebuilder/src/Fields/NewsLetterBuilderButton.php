<?php

namespace SolutionForest\PageBuilder\Fields;

use Encore\Admin\Admin;
use Encore\Admin\Form\Field;
use SolutionForest\PageBuilder\PageBuilder;

class NewsLetterBuilderButton extends Field
{
    protected $view = 'pagebuilder::newsletter';

    protected static $js = [
        '//cdn.ckeditor.com/4.5.10/standard/ckeditor.js',
        'vendor/laravel-admin-ext/pagebuilder/js/pagebuilder-btn.js',
        'vendor/laravel-admin-ext/pagebuilder/js/grapes.min.js',
        'vendor/laravel-admin-ext/pagebuilder/js/grapesjs-preset-newsletter.min.js',
    ];

    protected static $css = [
        'vendor/laravel-admin-ext/pagebuilder/css/pagebuilder.css',
        'vendor/laravel-admin-ext/pagebuilder/css/grapes.min.css',
        'vendor/laravel-admin-ext/pagebuilder/css/grapesjs-preset-newsletter.css',
    ];

    public function render(){
        $csrf = csrf_token();
        $setting = [
            "gjs-preset-newsletter"=>[
                "modalTitleImport" => 'Import template'
            ]
        ];
        $userSetting = PageBuilder::config("newsletter");
        if(!empty($userSetting) && is_array($userSetting)){
            $setting = array_merge($setting, $userSetting);
        }
        $settingJson = json_encode($setting);
        $id = str_replace("-", "_", $this->id);
        $this->script = <<<SCRIPT
        if(CKEDITOR.instances["{$id}"]){
            CKEDITOR.instances["{$id}"].destroy(true); 
        }
        CKEDITOR.replace('{$id}',{
            allowedContent: true,
            autoParagraph: false,
            removePlugins: 'image,pwimage'
        });
        $(document).off("click", "#builder-btn[data-field='{$id}']");
        $(document).on("click", "#builder-btn[data-field='{$id}']", function(event){
            event.preventDefault()
            var id = $(this).data("field");
            $(".page-builder-popup[data-field='"+id+"']").show();
            var editor = window["editor_"+id]
            console.log(editor)
            if(!editor){
                window.editor_{$id} = grapesjs.init({
                    container : '#gjs-{$id}',
                    plugins: ['gjs-preset-newsletter'],
                    pluginsOpts: {$settingJson},
                    storageManager: { type: 'none',},
                    assetManager:{
                        upload:"/admin/pagebuilder/assets/save",
                        headers:{"X-CSRF-TOKEN":"{$csrf}"}
                    }
                });

                $.getJSON("/admin/pagebuilder/images", function(response){
                    var am = window.editor_{$id}.AssetManager
                    if(response.data){
                        am.add(response.data)
                    }
                });

                window.editor_{$id}.Panels.addPanel({ id: "devices-c" }).get("buttons").add([
                    { id: "editor-save-btn", command: function(e, sender) {
                        var htmlWithCss = window.editor_{$id}.runCommand('gjs-get-inlined-html');
                        CKEDITOR.instances["{$id}"].setData(htmlWithCss);
                        console.log(htmlWithCss)
                        $(".page-builder-popup[data-field='{$id}']").hide(0, function(){
                            window.editor_{$id}.destroy();
                            window.editor_{$id} = null;
                            window.onbeforeunload = function() {
                                return null;
                            };
                        });
                        sender.set('active', false);
                    }, className: "fa fa-check-circle" },
                    { id: "editor-close-btn", command: function(e, sender) { 
                        if(confirm("Confirm to leave? Un-save data will lost")){
                            $(".page-builder-popup[data-field='{$id}']").hide(0, function(){
                                window.editor_{$id}.destroy();
                                window.editor_{$id} = null;
                                window.onbeforeunload = function() {
                                    return null;
                                };
                            });
                        }
                        sender.set('active', false);
                     }, className: "fa fa-times-circle" }
                ]);

                editor = window.editor_{$id}
            }
            var html = CKEDITOR.instances["{$id}"].getData();
            // console.log(html);
            window.editor_{$id}.setComponents(html)
        });
SCRIPT;
        return parent::render();
    }
}
