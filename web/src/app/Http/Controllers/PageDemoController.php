<?php

namespace App\Http\Controllers;

use App\Models\HtmlPage;
use Illuminate\Http\Request;

class PageDemoController extends Controller
{
    //
    public function page_demo($id){

        $html = HtmlPage::find($id)->page_html;
        return view('pageDemo',['html'=>$html]);
    }
}
