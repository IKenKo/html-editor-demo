#!/bin/bash

echo "Docker Login"
bash ./docker/docker-login.sh
echo "Docker build image"
docker build -f ./docker/Dockerfile -t sf/htmleditordemo:$1 .
docker tag sf/htmleditordemo:$1 sf/htmleditordemo:latest
echo "Docker upload to docker hub"
docker push sf/htmleditordemo:$1
docker push sf/htmleditordemo:latest
echo "Docker Logout"
docker logout
